import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../service/ApiService';
import { Http } from "@angular/http";
import { Geolocation, Geoposition } from '@ionic-native/geolocation';

declare var google;

@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {

  private api: ApiService;
  private actualPosition: any;
  public map: any;
  private directionsService: any = null;
  private directionsDisplay: any = null;
  private bounds: any = null;
  private swGo:boolean;
  private markerATM:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http, private geolocation: Geolocation) {
    this.api = new ApiService(http);
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.bounds = new google.maps.LatLngBounds();
    this.swGo=false;

  }

  ionViewDidLoad() {
    // load position and map
    this.geolocation.getCurrentPosition()
      .then(response => {
        this.loadMap(response);
      })
      .catch(error => {
        console.log(error);
      })
  }

  public loadMap(position: Geoposition) {

    this.actualPosition = { lat: position.coords.latitude, lng: position.coords.longitude };
    let mapEle: HTMLElement = document.getElementById('map');
    this.map = new google.maps.Map(mapEle, {
      center: { lat: position.coords.latitude, lng: position.coords.longitude },
      zoom: 12
    });
    this.directionsDisplay.setMap(this.map);
    this.loadATM();

  }

  public loadATM() {
    this.api.getAllATM().subscribe(data => {
      console.log(data);
      data.forEach(atm => {
        var point = new google.maps.LatLng(atm.location.lat, atm.location.lon);
        this.addATM(point, atm.name);
      });//end foreach
    });//end api
  }

  public addATM(position, title) {
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: position,
      icon: "assets/imgs/atm.png"
    });
    this.bounds.extend(position);
    this.addClickEvent(marker, title);
    return marker;
  }

  public addMarkerPosition() {
    new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: this.actualPosition,
      icon: "assets/imgs/man.png"
    });
    this.bounds.extend(this.actualPosition);
  }

  public addClickEvent(marker, content) {
    google.maps.event.addListener(marker, 'click', () => {
      this.markerATM=marker.position;
      this.swGo=true;
      this.addMarkerPosition();
    });
  }

  private goRute(){
    this.calculateRoute(this.markerATM);
    this.swGo=false;
  }


  private calculateRoute(atm) {
    
    this.directionsService.route({
      origin: new google.maps.LatLng(this.actualPosition.lat, this.actualPosition.lng),
      destination: new google.maps.LatLng(atm.lat(), atm.lng()),
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING,
      avoidTolls: true
    }, (response, status) => {
      if (status === google.maps.DirectionsStatus.OK) {
        this.directionsDisplay.setDirections(response);
      } else {
        alert('Error' + status);
      }
    });

  }

}

