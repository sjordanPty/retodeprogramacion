import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {LocationPage} from '../location/location';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {}

  public locationPage(){
    this.navCtrl.push(LocationPage);
  }

}
