import { Http, Response } from "@angular/http";
import 'rxjs/Rx';
export class ApiService{
    
    public server: String;

    constructor(public http: Http) {}

    public getAllATM() {
        //local
        //return this.http.get("branch.json").map((res: Response) => res.json());
        //remoto heroku
        return this.http.get("https://retodeprogramacion.herokuapp.com/").map((res: Response) => res.json());
    }
}